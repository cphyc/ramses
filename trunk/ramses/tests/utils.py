from tempfile import mkdtemp
import sh
from shutil import copy
import unittest
from functools import wraps
import os
from os.path import join
import shutil

RAMSES2D='/automnt/data74/cadiou/ramses/trunk/ramses/bin/ramses_patched2d'
RAMSES3D='/automnt/data74/cadiou/ramses/trunk/ramses/bin/ramses_patched3d'

ramses2d = sh.Command(RAMSES2D)
ramses3d = sh.Command(RAMSES3D)

def build(ndim=2):
    raise NotImplementedError()

def fill_data(fun, *args, **kwargs):
    @wraps(fun)
    def wrap(obj, *args, **kwargs):
        return obj.run_test(fun)

    return wrap

class BaseSimulManager(unittest.TestCase):
    _wdir = None
    require_files = []

    def simul_init(self, callback=None, verbose=False,
                   wdir=None, *args, **kwargs):
        self.callback = callback
        self._wdir = wdir if wdir is not None else self._wdir
        self.verbose = verbose

        self.copy_files()
        # Call the initialization block
        self.initialize()

        return self

    def copy_files(self):
        for f in self.require_files:
            if self.verbose:
                print('=> copying %s into %s' % (f, self.cwd))
            copy(f, self.cwd)

    @property
    def simul_data(self):
        '''Lazy loading of the data. It also runs the simulation.'''
        if not self.runned:
            if self.verbose:
                print('=> running in directory %s' % self.cwd)
            self.run_simulation()
            sh.touch(join(self.cwd, '.runned'))

        if not hasattr(self, '_data'):
            if self.verbose:
                print('=> reading data from files')
            self._data = self.read_data()

        return self._data

    @property
    def cwd(self):
        '''Lazy create a temporary working directory'''
        if self._wdir is None:
            print('=> creating working directory')
            self._wdir = self.create_cwd()

        elif not os.path.isdir(self._wdir):
            os.mkdir(self._wdir)

        return self._wdir

    @staticmethod
    def create_cwd():
        return mkdtemp(dir='.')

    @property
    def runned(self):
        return os.path.isfile(join(self.cwd, '.runned'))


    def initialize(self):
        '''
        This method is called once and should setup the environment to run a
        simulation
        '''
        raise NotImplementedError('Initialize not implemented')

    def run_simulation(self):
        '''
        This method is called on request once and should run a simulation.
        Return errorcode (if any).
        '''
        raise NotImplementedError('Run not implemented')

    def read_data(self):
        '''
        This method is called once to read and load the data.
        '''
        raise NotImplementedError('Read data not implemented')

    def clean(self):
        '''
        This method is called after all request and should cleanup a simulation
        '''
        raise NotImplementedError('Clean not implemented')

