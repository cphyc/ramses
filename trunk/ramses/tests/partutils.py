from scipy.io import FortranFile as FF
from glob import glob
import os
import numpy as np
from tqdm import tqdm

#################################
# Read helpers
#################################
def read_one_cpu(output):
    f = FF(output)
    ncpu = f.read_ints()[0]
    ndim = f.read_ints()[0]
    npart = f.read_ints()[0]
    localseed = f.read_ints()[0]
    nstart_tot = f.read_ints()[0]
    mstar_tot = f.read_ints()[0]
    mstart_lost = f.read_ints()[0]
    nsink = f.read_ints()[0]

    x = np.zeros((ndim, npart), dtype=float)
    v = np.zeros((ndim, npart), dtype=float)
    m = np.zeros((npart), dtype=float)
    ind = np.zeros((npart), dtype=int)
    for dim in range(ndim):
        x[dim] = f.read_reals()
    for dim in range(ndim):
        v[dim] = f.read_reals()

    m = f.read_reals()
    ind = f.read_ints()
    lvl = f.read_ints()
    tp = f.read_reals()
    fam = f.read_ints()

    return ind, ndim, npart, x, v, m, lvl, tp, fam


def read_output(path):
    paths = glob(os.path.join(path, 'part_*'))

    _pos = [None]*len(paths)
    _vel = [None]*len(paths)
    _mass = [None]*len(paths)
    _lvl = [None]*len(paths)
    _ind = [None]*len(paths)
    _npart = [None]*len(paths)
    _fam = [None]*len(paths)
    _tp = [None]*len(paths)
    _cpus = [None]*len(paths)
    npart = 0
    for i, output in enumerate(paths):
        (_ind[i], ndim, _npart[i], _pos[i], _vel[i], _mass[i],
         _lvl[i], _tp[i], _fam[i]) = read_one_cpu(output)

        cpu = int(output.split('.out')[-1])
        _cpus[i] = cpu
        npart += _npart[i]

    # # Prevent bug when there is no particle
    # if i == 0:
    #     ndim = 1
    pos = np.zeros((ndim, npart))
    vel = np.zeros((ndim, npart))
    mass = np.zeros((npart))
    ind = np.zeros((npart), dtype=int)
    lvl = np.zeros((npart), dtype=int)
    cpus = np.zeros((npart), dtype=int)
    fam = np.zeros((npart), dtype=int)

    n = 0
    for i in range(len(paths)):
        ids = _ind[i] - 1
        pos[:, ids] = _pos[i]
        vel[:, ids] = _vel[i]
        ind[n:n+_npart[i]] = ids

        mass[ids] = _mass[i]
        isTracer = ids[_mass[i] == 0]
        mass[isTracer] = _tp[i]

        lvl[ids] = _lvl[i]
        cpus[ids] = _cpus[i]

        fam[ids] = _fam[i]

        n += _npart[i]

    return ind, pos, vel, mass, lvl, cpus

def read_outputs(path='.'):
    paths = sorted(glob(os.path.join(path, 'output_*')))
    return ((output, read_output(output)) for output in paths)
