import f90nml
import sh
from os.path import join
from utils import *
from partutils import read_outputs
import numpy as np

import nose

class RamsesTracerSingleNoAmrTest(BaseSimulManager):
    '''Check that the code is working using a single refinment domain and one CPU'''
    def initialize(self):
        vals = dict(
            run_params=dict(
                hydro=True,
                pic=True,
                nrestart=0,
                ncontrol=10,
                nremap=10,
                nsubcycle=[1]*10,
                debug=True),
            tracer_params=dict(
                tracer=True,
                MC_tracer=True,
                tracerfile='kh_ic_lvl7_1percell_noKH',
                with_kh=True),
            amr_params=dict(
                levelmin=7,
                levelmax=7,
                ngridmax=6000,
                npartmax=100000,
                nexpand=1,
                boxlen=1.0),
            boundary_params=dict(
                nboundary = 2,
                ibound_min=[-1,1],
                ibound_max=[-1,1],
                bound_type=[2, 2]),
            init_params=dict(
                nregion=2,
                d_region=[2.0,1.0],
                v_region=[+1.0,-1.0],
                x_center=0.5),
            output_params=dict(
                foutput=1,
                noutput=1,
                tout=0.1),
            hydro_params=dict(
                gamma=1.4,
                courant_factor=0.8,
                slope_type=2,
                scheme='muscl'),
            refine_params=dict(
                err_grad_d=0.01,
                interpol_type=1)
            )
        nml = f90nml.Namelist(vals)

        if self.verbose:
            print('=> writing namelist')
        self.namelist = 'namelist.nml'
        nml.write(join(self.cwd, self.namelist), force=True)
        self.nml = nml

    def run_simulation(self):
        ramses2d(self.namelist,
                 _cwd=self.cwd,
                 _out=join(self.cwd, 'log'))

    def read_data(self):
        return [dict(output=output, ind=ind, pos=pos, vel=vel,
                     mass=mass, lvl=lvl, cpus=cpus)
                for output, (ind, pos, vel, mass, lvl, cpus)
                in read_outputs(self.cwd)]

    # Test specific environment
    @classmethod
    def setUpClass(cls):
        # set up a working directory
        cls._wdir = 'single_noAMR_noKH' #cls.create_cwd()
        cls.require_files = ['kh_ic_lvl7_1percell_noKH']

    def setUp(self):
        self.simul_init(verbose=True)

    def cell_to_cell_test(self):
        """Check the particles move from one cell to another one
        """
        data = self.simul_data
        levelmin = self.nml['amr_params']['levelmin']
        dx = 1./2**levelmin
        for i, (data1, data2) in enumerate(zip(data[1:-1], data[2:])):
            pos1 = data1['pos']
            pos2 = data2['pos']

            dxpdy = np.sum(np.abs(pos2-pos1), axis=0)
            barr = np.logical_or(dxpdy == dx,
                                 dxpdy == 0)
            barr = np.logical_or(barr,
                                 dxpdy == 1-dx)

            print('at step %s' % i)

            print(dx, dxpdy)
            self.assertTrue(np.all(barr))

    def cell_to_cell_test(self):
        """Check the particles move in 1 direction
        """
        data = self.simul_data
        levelmin = self.nml['amr_params']['levelmin']
        dx = 1./2**levelmin
        for i, (data1, data2) in enumerate(zip(data[1:-1], data[2:])):
            pos1 = data1['pos']
            pos2 = data2['pos']

            dxdy = np.abs(np.product(pos2-pos1, axis=0))
            self.assertEqual(dxdy.max(), 0)

    def distribution_test(self):
        """Check that the distribution is correct
        """
        data = self.simul_data
        levelmin = self.nml['amr_params']['levelmin']
        for dt in data:
            pos = dt['pos']
            # project on a grid
            H, _, _ = np.histogram2d(pos[:, 0], pos[:, 1], bins=2**(levelmin-1))
            mean = H.mean()
            std = H.std()
            expstd = np.sqrt(mean)

            # Check that the standard deviation is roughly equal to the square root of
            # the mean number of particles in each cell (+-10^-3)
            self.assertAlmostEqual(std/expstd, 1, 3)

    # There should be twice as much going down than going up
    def up_and_down_test(self):
        data = self.simul_data
        diff = np.zeros(len(data)-1)
        for i, (data1, data2) in enumerate(zip(data[1:-1], data[2:])):
            pos1 = data1['pos']
            pos2 = data2['pos']

            dy = pos2[1, :] - pos1[1, :]
            up   = dy > 0
            down = dy < 0

            # The number of up should be #up =~ 2 * #down
            nup = up.sum()
            ndown = down.sum()

            diff[i] = nup - ndown

        # There are 4096 particles
        dev1 = np.sqrt(4096)
        # We do a random gaussian walk of expected deviation
        devall = dev1 / np.sqrt(len(diff))

        # We give some freedom here
        print(diff.mean(), devall)

        # Check the mean is smaller than the deviation (this is very crude)
        self.assertLess(np.abs(diff.mean()), 2*devall)
