module pm_commons
  use amr_parameters
  use pm_parameters
  use random

  implicit none

  ! Sink particle related arrays
  real(dp),allocatable,dimension(:)::msink,r2sink,v2sink,c2sink,oksink_new,oksink_all,tsink
  real(dp),allocatable,dimension(:)::msink_new,msink_all,r2k,v2sink_new,c2sink_new,tsink_new,tsink_all
  real(dp),allocatable,dimension(:)::v2sink_all,c2sink_all
  real(dp),allocatable,dimension(:)::dMBHoverdt,dMEdoverdt,wdens,wvol,wc2
  real(dp),allocatable,dimension(:)::wdens_new,wvol_new,wc2_new,total_volume
  real(dp),allocatable,dimension(:,:)::wmom,wmom_new
  real(dp),allocatable,dimension(:,:)::vsink,vsink_new,vsink_all
  real(dp),allocatable,dimension(:,:)::xsink,xsink_new,xsink_all
  real(dp),allocatable,dimension(:,:)::weighted_density,weighted_volume,weighted_c2
  real(dp),allocatable,dimension(:,:)::jsink,jsink_new,jsink_all
  real(dp),allocatable,dimension(:)::dMBH_coarse,dMEd_coarse,dMsmbh,dMBH_coarse_new
  real(dp),allocatable,dimension(:)::dMEd_coarse_new,dMsmbh_new,dMBH_coarse_all,dMEd_coarse_all,dMsmbh_all
  real(dp),allocatable,dimension(:)::Esave,Esave_new,Esave_all
  real(dp),allocatable,dimension(:,:,:)::weighted_momentum
  real(dp),allocatable,dimension(:,:,:)::sink_stat,sink_stat_all
  real(dp),allocatable,dimension(:)::c_avgptr,v_avgptr,d_avgptr
  real(dp),allocatable,dimension(:)::spinmag,spinmag_new,spinmag_all
  real(dp),allocatable,dimension(:,:)::bhspin,bhspin_new,bhspin_all
  real(dp),allocatable,dimension(:)::eps_sink
  integer ,allocatable,dimension(:)::idsink,idsink_new,idsink_all
  integer::nindsink=0


  ! Particles related arrays
  real(dp),allocatable,dimension(:,:):: xp       ! Positions
  real(dp),allocatable,dimension(:,:):: vp       ! Velocities
  real(dp),allocatable,dimension(:)  :: mp       ! Masses
  logical,allocatable,dimension(:)   :: move_flag! Move flag (for particles)
  real(dp),allocatable,dimension(:)  :: mp0      ! Initial masses (for Kimm feedback)
#ifdef OUTPUT_PARTICLE_POTENTIAL
  real(dp),allocatable,dimension(:)  ::ptcl_phi  ! Potential of particle added by AP for output purposes
#endif
#ifdef NTRACEGROUPS
  integer ,allocatable,dimension(:)  ::ptracegroup
#endif
  real(dp),allocatable,dimension(:)  :: tp       ! Birth epoch
  real(dp),allocatable,dimension(:,:):: weightp  ! weight of cloud parts for sink accretion only
  real(dp),allocatable,dimension(:)  :: zp       ! Birth metallicity
  real(dp),allocatable,dimension(:)  :: tmpp     ! Working array
  integer, allocatable,dimension(:):: itmpp     ! Working array
  integer, allocatable,dimension(:)  :: partp    ! Particle parent (for tracers only)
  real(dp),allocatable,dimension(:)  ::st_n_tp  ! Gas density at birth epoch         !SD
  real(dp),allocatable,dimension(:)  ::st_n_sn  ! Gas density at SN epoch            !SD
  real(dp),allocatable,dimension(:)  ::st_e_sn  ! SN energy injected                 !SD
  !!! Add particle family !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  integer ,allocatable,dimension(:)  :: famp     ! Particle family
  integer, parameter :: FDM=0, FSTAR=1, FSTAR_DEAD=2, FSTAR_OLD=3
  integer, parameter :: FDEBRIS=-1, FSINK_CLOUD=10
  integer, parameter :: FTRACER=100,  FTRACER_STAR=101, FTRACER_SINK=102, FTRACER_CLASSIC=103, FUNSET=-2, FTRACER_RELEASED=110
  ! Note on the values for the family:
  !---------------------------!
  !  Particle type  !  Value  !
  !-----------------+---------!
  !        DM       !     0   !
  !       Star      !     1   !
  !  Star (remnant) !     2   !
  !   Star (old)    !     3   !
  !      Debris     !    -1   !
  !       Sink      !    10   !
  ! Tracers on grid !   100+  ! (any family above 100 is a tracer)
  !      Unset      !    -2   !
  !      Other      !   ...   !
  !---------------------------!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  integer ,allocatable,dimension(:)  ::nextp     ! Next particle in list
  integer ,allocatable,dimension(:)  ::prevp     ! Previous particle in list
  integer ,allocatable,dimension(:)  ::levelp    ! Current level of particle
  integer(i8b),allocatable,dimension(:)::idp     ! Identity of particle
  ! Tree related arrays
  integer ,allocatable,dimension(:)   ::headp    ! Head particle in grid
  integer ,allocatable,dimension(:)   ::tailp    ! Tail particle in grid
  integer ,allocatable,dimension(:)   ::numbp    ! Number of particles in grid
  ! Global particle linked lists
  integer::headp_free,tailp_free,numbp_free=0,numbp_free_tot=0
  ! Local and current seed for random number generator
  integer,dimension(IRandNumSize) :: localseed=-1

contains
  function cross(a,b)
    use amr_parameters, only:dp
    real(dp),dimension(1:3)::a,b
    real(dp),dimension(1:3)::cross
    !computes the cross product c= a x b
    cross(1)=a(2)*b(3)-a(3)*b(2)
    cross(2)=a(3)*b(1)-a(1)*b(3)
    cross(3)=a(1)*b(2)-a(2)*b(1)
  end function cross

  logical elemental function is_tracer(fam)
    integer, intent(in) :: fam
    is_tracer = (fam >= FTRACER)
  end function is_tracer

  logical elemental function is_MC_tracer(fam)
    integer, intent(in) :: fam
    is_MC_tracer = (fam == FTRACER)
  end function is_MC_tracer

  logical elemental function is_classical_tracer(fam)
    integer, intent(in) :: fam
    is_classical_tracer = (fam == FTRACER_CLASSIC)
  end function is_classical_tracer

  logical elemental function is_star_tracer(fam)
    integer, intent(in) :: fam
    is_star_tracer = (fam == FTRACER_STAR)
  end function is_star_tracer

  logical elemental function is_sink_tracer(fam)
    integer, intent(in) :: fam
    is_sink_tracer = (fam == FTRACER_SINK)
  end function is_sink_tracer


  logical elemental function is_star(fam)
    integer, intent(in) :: fam
    is_star = (fam >= FSTAR .and. fam <= FSTAR_OLD)
  end function is_star

  logical elemental function is_old_star(fam)
    integer, intent(in) :: fam
    is_old_star = (fam == FSTAR_OLD)
  end function is_old_star

  logical elemental function is_dead_star(fam)
    integer, intent(in) :: fam
    is_dead_star = (fam == FSTAR_DEAD)
  end function is_dead_star

  logical elemental function is_active_star(fam)
    integer, intent(in) :: fam
    is_active_star = (fam == FSTAR)
  end function is_active_star


  logical elemental function is_DM(fam)
    integer, intent(in) :: fam
    is_DM = (fam == FDM)
  end function is_DM

  logical elemental function is_sink_cloud(fam)
    integer, intent(in) :: fam
    is_sink_cloud = (fam == FSINK_CLOUD)
  end function is_sink_cloud

end module pm_commons
