subroutine move_fine(ilevel)
  use amr_commons
  use pm_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !----------------------------------------------------------------------
  ! Update particle position and time-centred velocity at level ilevel.
  ! If particle sits entirely in level ilevel, then use fine grid force
  ! for CIC interpolation. Otherwise, use coarse grid (ilevel-1) force.
  !----------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,ig,ip,npart1,info,isink
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Set new sink variables to old ones
  if(sink)then
     vsink_new=0d0
     oksink_new=0d0
     sink_stat(:,ilevel,:)=0d0
  endif

  ! Update particles position and velocity
  ig=0
  ip=0

  ! Loop over grids for not tracers
  igrid=headl(myid,ilevel)
  do jgrid=1,numbl(myid,ilevel)
     npart1=numbp(igrid)  ! Number of particles in the grid
     if(npart1>0)then
        ig=ig+1
        ind_grid(ig) = igrid
        ipart=headp(igrid)
        ! Loop over particles
        do jpart=1,npart1
           ! Save next particle  <---- Very important !!!
           next_part=nextp(ipart)
           ! Classical particles
           if(ig==0)then
              ig=1
              ind_grid(ig)=igrid
           end if
           if (MC_tracer .and. is_tracer(famp(ipart)) .and. &
                famp(ipart) /= FTRACER_CLASSIC) then
              ! MC Tracer, will do it later
           else
              ip=ip+1
              ind_part(ip)=ipart
              ind_grid_part(ip)=ig
              if(ip==nvector .or. ig==nvector)then
                 call move1(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
           end if

           ! MC Tracer patch
           ! Reset grid counter if tracer particles only
           if (ig>0 .and. ip==0) then
              ig = 0
           end if
           ! End MC Tracer patch
           ipart=next_part  ! Go to next particle
        end do
        ! End loop over particles
        if (ig==nvector)then
           call move1(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
           ip=0
           ig=0
        end if
     end if
     igrid=next(igrid)   ! Go to next grid
  end do

  if(ip>0) call move1(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)

  if (mc_tracer) then  ! Loop over grids for MC tracers
     ig=0
     ip=0
     ind_grid=0
     ind_part=0
     ind_grid_part=0

     igrid=headl(myid,ilevel)
     do jgrid=1,numbl(myid,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        if(npart1>0)then
           ig=ig+1

           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle  <---- Very important !!!
              next_part=nextp(ipart)
              ! Classical particles
              if(ig==0)then
                 ig=1
                 ind_grid(ig)=igrid
              end if
              if (is_tracer(famp(ipart)) .and. famp(ipart) /= FTRACER_CLASSIC) then
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig
                 if(ip==nvector)then
                    call move_tracer(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                    ip=0
                    ig=0
                 end if
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles

           if(ig==nvector)then
              call move_tracer(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
              ip=0
              ig=0
           end if

        end if
        igrid=next(igrid)   ! Go to next grid
     end do
     ! End loop over grids
     if(ip>0) call move_tracer(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel) ! MC Tracer
  end if

  if(sink)then
     if(nsink>0)then
#ifndef WITHOUTMPI
        call MPI_ALLREDUCE(oksink_new,oksink_all,nsinkmax     ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
        call MPI_ALLREDUCE(vsink_new ,vsink_all ,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
#else
        oksink_all=oksink_new
        vsink_all=vsink_new
#endif
     endif
     do isink=1,nsink
        if(oksink_all(isink)==1d0)then
           vsink(isink,1:ndim)=vsink_all(isink,1:ndim)
           xsink(isink,1:ndim)=xsink(isink,1:ndim)+vsink(isink,1:ndim)*dtnew(ilevel)
        endif
     end do
  endif

111 format('   Entering move_fine for level ',I2)

end subroutine move_fine
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine move_fine_static(ilevel)
  use amr_commons
  use pm_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !----------------------------------------------------------------------
  ! Update particle position and time-centred velocity at level ilevel.
  ! If particle sits entirely in level ilevel, then use fine grid force
  ! for CIC interpolation. Otherwise, use coarse grid (ilevel-1) force.
  !----------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,ig,ip,npart1,npart2
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part
  integer :: info, isink

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Update particles position and velocity
  ig=0
  ip=0
  ! Loop over grids
  igrid=headl(myid,ilevel)
  do jgrid=1,numbl(myid,ilevel)
     npart1=numbp(igrid)  ! Number of particles in the grid
     npart2=0

     ! Count particles
     if(npart1>0)then
        ipart=headp(igrid)
        ! Loop over particles
        do jpart=1,npart1
           ! Save next particle   <--- Very important !!!
           next_part=nextp(ipart)
           if(star) then
              !!! Add family !!!
              if((.not.static_dm.and.tp(ipart).eq.0).or.(.not.static_stars.and.tp(ipart).ne.0)) then
              !if((.not.static_dm.and.famp(ipart).eq.0).or.(.not.static_stars.and.famp(ipart).ne.0)) then
              ! Keep initial implementation, but weird (initial stars count as DM)
              !!!!!!!!!!!!!!!!!!
                 npart2=npart2+1
              endif
           else
              if(.not.static_dm) then
                 npart2=npart2+1
              endif
           endif
           ipart=next_part  ! Go to next particle
        end do
     endif

     ! Gather star particles
     if(npart2>0)then
        ig=ig+1
        ind_grid(ig)=igrid
        ipart=headp(igrid)
        ! Loop over particles
        do jpart=1,npart1
           ! Save next particle   <--- Very important !!!
           next_part=nextp(ipart)
           ! Select particles
           if(star) then
              !!! Add family !!!
              if((.not.static_dm.and.tp(ipart).eq.0).or.(.not.static_stars.and.tp(ipart).ne.0)) then
              !if((.not.static_dm.and.famp(ipart).eq.0).or.(.not.static_stars.and.famp(ipart).ne.0)) then
              ! Keep initial implementation, but weird (initial stars count as DM)
              !!!!!!!!!!!!!!!!!!
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig
              endif
           else
              if(.not.static_dm) then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig
              endif
           endif
           if(ip==nvector)then
              call move1(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
              ip=0
              ig=0
           end if
           ipart=next_part  ! Go to next particle
        end do
        ! End loop over particles
     end if
     igrid=next(igrid)   ! Go to next grid
  end do
  ! End loop over grids
  if(ip>0)call move1(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)

  if(sink)then
     if(nsink>0)then
#ifndef WITHOUTMPI
        call MPI_ALLREDUCE(oksink_new,oksink_all,nsinkmax     ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
        call MPI_ALLREDUCE(vsink_new ,vsink_all ,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
#else
        oksink_all=oksink_new
        vsink_all=vsink_new
#endif
     endif
     do isink=1,nsink
        if(oksink_all(isink)==1d0)then
           vsink(isink,1:ndim)=vsink_all(isink,1:ndim)
           xsink(isink,1:ndim)=xsink(isink,1:ndim)+vsink(isink,1:ndim)*dtnew(ilevel)
        endif
     end do
  endif


111 format('   Entering move_fine for level ',I2)

end subroutine move_fine_static
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine move1(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use poisson_commons
  use hydro_commons, ONLY: uold,smallr,gamma
  use cooling_module, ONLY: XH=>X, rhoc, mH
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !------------------------------------------------------------
  ! This routine computes the force on each particle by
  ! inverse CIC and computes new positions for all particles.
  ! If particle sits entirely in fine level, then CIC is performed
  ! at level ilevel. Otherwise, it is performed at level ilevel-1.
  ! This routine is called by move_fine.
  !------------------------------------------------------------
  logical::error
  integer::i,j,ind,idim,nx_loc,isink
  real(dp)::dx,length,dx_loc,scale,vol_loc,r2
  ! Grid-based arrays
  integer ,dimension(1:nvector),save::father_cell
  real(dp),dimension(1:nvector,1:ndim),save::x0
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids
  ! Particle-based arrays
  logical ,dimension(1:nvector),save::ok
  real(dp),dimension(1:nvector,1:ndim),save::x,ff,new_xp,new_vp,dd,dg
  integer ,dimension(1:nvector,1:ndim),save::ig,id,igg,igd,icg,icd
  real(dp),dimension(1:nvector,1:twotondim),save::vol
  integer ,dimension(1:nvector,1:twotondim),save::igrid,icell,indp,kg
  real(dp),dimension(1:3)::xbound,skip_loc
  integer ::nlevelmax_loc
  real(dp)::dx_min,vol_min,dx_temp,dx_min_loc
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::d0,mstar,nISM,nCOM,e,d,u,v,w,ekk
  real(dp)::xx

  ! Family
  logical,dimension(1:nvector),save :: classical_tracer

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Mesh spacing in that level
  dx=0.5D0**ilevel
  xbound(1:3)=(/dble(nx),dble(ny),dble(nz)/)
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**3

  ! Finest cell size
  dx_min=(0.5D0**nlevelmax)*scale
  vol_min=dx_min**ndim
  ! Typical ISM mass density from H/cc to code units
  nISM = n_star
  nCOM = del_star*omega_b*rhoc/aexp**3*XH/mH
  nISM = MAX(nCOM,nISM)
  d0   = nISM/scale_nH
  ! Star particle mass
  mstar=MAX(del_star*omega_b*rhoc*XH/mH,n_star)/(scale_nH*aexp**3)*vol_min
  do i=1,nlevelmax
     dx_temp=scale*0.5D0**i
     ! Test is designed so that nlevelmax is activated at aexp \simeq 0.8
     if(d0*(dx_temp/2.0)**ndim.ge.mstar/2d0)nlevelmax_loc=i+1
  enddo
  dx_min_loc=scale*0.5d0**nlevelmax_loc

  ! Lower left corner of 3x3x3 grid-cube
  do idim=1,ndim
     do i=1,ng
        x0(i,idim)=xg(ind_grid(i),idim)-3.0D0*dx
     end do
  end do

  ! Gather neighboring father cells (should be present anytime !)
  do i=1,ng
     father_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(father_cell,nbors_father_cells,nbors_father_grids,&
       & ng,ilevel)

  ! Rescale particle position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-x0(ind_grid_part(j),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  ! Check for illegal moves
  error=.false.
  do idim=1,ndim
     do j=1,np
        if(x(j,idim)<0.5D0.or.x(j,idim)>5.5D0)error=.true.
     end do
  end do
  if(error)then
     write(*,*)'problem in move'
     do idim=1,ndim
        do j=1,np
           if(x(j,idim)<0.5D0.or.x(j,idim)>5.5D0)then
              write(*,*)x(j,1:ndim)
           endif
        end do
     end do
     stop
  end if

  ! CIC at level ilevel (dd: right cloud boundary; dg: left cloud boundary)
  do idim=1,ndim
     do j=1,np
        dd(j,idim)=x(j,idim)+0.5D0
        id(j,idim)=int(dd(j,idim))
        dd(j,idim)=dd(j,idim)-id(j,idim)
        dg(j,idim)=1.0D0-dd(j,idim)
        ig(j,idim)=id(j,idim)-1
     end do
  end do

   ! Compute parent grids
  do idim=1,ndim
     do j=1,np
        igg(j,idim)=ig(j,idim)/2
        igd(j,idim)=id(j,idim)/2
     end do
  end do
#if NDIM==1
  do j=1,np
     kg(j,1)=1+igg(j,1)
     kg(j,2)=1+igd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     kg(j,1)=1+igg(j,1)+3*igg(j,2)
     kg(j,2)=1+igd(j,1)+3*igg(j,2)
     kg(j,3)=1+igg(j,1)+3*igd(j,2)
     kg(j,4)=1+igd(j,1)+3*igd(j,2)
  end do
#endif
#if NDIM==3
  do j=1,np
     kg(j,1)=1+igg(j,1)+3*igg(j,2)+9*igg(j,3)
     kg(j,2)=1+igd(j,1)+3*igg(j,2)+9*igg(j,3)
     kg(j,3)=1+igg(j,1)+3*igd(j,2)+9*igg(j,3)
     kg(j,4)=1+igd(j,1)+3*igd(j,2)+9*igg(j,3)
     kg(j,5)=1+igg(j,1)+3*igg(j,2)+9*igd(j,3)
     kg(j,6)=1+igd(j,1)+3*igg(j,2)+9*igd(j,3)
     kg(j,7)=1+igg(j,1)+3*igd(j,2)+9*igd(j,3)
     kg(j,8)=1+igd(j,1)+3*igd(j,2)+9*igd(j,3)
  end do
#endif
  do ind=1,twotondim
     do j=1,np
        igrid(j,ind)=son(nbors_father_cells(ind_grid_part(j),kg(j,ind)))
     end do
  end do

  ! Check if particles are entirely in level ilevel
  ok(1:np)=.true.
  do ind=1,twotondim
     do j=1,np
        ok(j)=ok(j).and.igrid(j,ind)>0
     end do
  end do

  ! If not, rescale position at level ilevel-1
  do idim=1,ndim
     do j=1,np
        if(.not.ok(j))then
           x(j,idim)=x(j,idim)/2.0D0
        end if
     end do
  end do
  ! If not, redo CIC at level ilevel-1
  do idim=1,ndim
     do j=1,np
        if(.not.ok(j))then
           dd(j,idim)=x(j,idim)+0.5D0
           id(j,idim)=int(dd(j,idim))
           dd(j,idim)=dd(j,idim)-id(j,idim)
           dg(j,idim)=1.0D0-dd(j,idim)
           ig(j,idim)=id(j,idim)-1
        end if
     end do
  end do

 ! Compute parent cell position
  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           icg(j,idim)=ig(j,idim)-2*igg(j,idim)
           icd(j,idim)=id(j,idim)-2*igd(j,idim)
        else
           icg(j,idim)=ig(j,idim)
           icd(j,idim)=id(j,idim)
        end if
     end do
  end do
#if NDIM==1
  do j=1,np
     icell(j,1)=1+icg(j,1)
     icell(j,2)=1+icd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     if(ok(j))then
        icell(j,1)=1+icg(j,1)+2*icg(j,2)
        icell(j,2)=1+icd(j,1)+2*icg(j,2)
        icell(j,3)=1+icg(j,1)+2*icd(j,2)
        icell(j,4)=1+icd(j,1)+2*icd(j,2)
     else
        icell(j,1)=1+icg(j,1)+3*icg(j,2)
        icell(j,2)=1+icd(j,1)+3*icg(j,2)
        icell(j,3)=1+icg(j,1)+3*icd(j,2)
        icell(j,4)=1+icd(j,1)+3*icd(j,2)
     end if
  end do
#endif
#if NDIM==3
  do j=1,np
     if(ok(j))then
        icell(j,1)=1+icg(j,1)+2*icg(j,2)+4*icg(j,3)
        icell(j,2)=1+icd(j,1)+2*icg(j,2)+4*icg(j,3)
        icell(j,3)=1+icg(j,1)+2*icd(j,2)+4*icg(j,3)
        icell(j,4)=1+icd(j,1)+2*icd(j,2)+4*icg(j,3)
        icell(j,5)=1+icg(j,1)+2*icg(j,2)+4*icd(j,3)
        icell(j,6)=1+icd(j,1)+2*icg(j,2)+4*icd(j,3)
        icell(j,7)=1+icg(j,1)+2*icd(j,2)+4*icd(j,3)
        icell(j,8)=1+icd(j,1)+2*icd(j,2)+4*icd(j,3)
     else
        icell(j,1)=1+icg(j,1)+3*icg(j,2)+9*icg(j,3)
        icell(j,2)=1+icd(j,1)+3*icg(j,2)+9*icg(j,3)
        icell(j,3)=1+icg(j,1)+3*icd(j,2)+9*icg(j,3)
        icell(j,4)=1+icd(j,1)+3*icd(j,2)+9*icg(j,3)
        icell(j,5)=1+icg(j,1)+3*icg(j,2)+9*icd(j,3)
        icell(j,6)=1+icd(j,1)+3*icg(j,2)+9*icd(j,3)
        icell(j,7)=1+icg(j,1)+3*icd(j,2)+9*icd(j,3)
        icell(j,8)=1+icd(j,1)+3*icd(j,2)+9*icd(j,3)
     end if
  end do
#endif

  ! Compute parent cell adresses
  do ind=1,twotondim
     do j=1,np
        if(ok(j))then
           indp(j,ind)=ncoarse+(icell(j,ind)-1)*ngridmax+igrid(j,ind)
        else
           indp(j,ind)=nbors_father_cells(ind_grid_part(j),icell(j,ind))
        end if
     end do
  end do

  ! Compute cloud volumes
#if NDIM==1
  do j=1,np
     vol(j,1)=dg(j,1)
     vol(j,2)=dd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     vol(j,1)=dg(j,1)*dg(j,2)
     vol(j,2)=dd(j,1)*dg(j,2)
     vol(j,3)=dg(j,1)*dd(j,2)
     vol(j,4)=dd(j,1)*dd(j,2)
  end do
#endif
#if NDIM==3
  do j=1,np
     vol(j,1)=dg(j,1)*dg(j,2)*dg(j,3)
     vol(j,2)=dd(j,1)*dg(j,2)*dg(j,3)
     vol(j,3)=dg(j,1)*dd(j,2)*dg(j,3)
     vol(j,4)=dd(j,1)*dd(j,2)*dg(j,3)
     vol(j,5)=dg(j,1)*dg(j,2)*dd(j,3)
     vol(j,6)=dd(j,1)*dg(j,2)*dd(j,3)
     vol(j,7)=dg(j,1)*dd(j,2)*dd(j,3)
     vol(j,8)=dd(j,1)*dd(j,2)*dd(j,3)
  end do
#endif

  ! Boolean flag for classical tracers
  do j = 1, np
     classical_tracer(j) = is_classical_tracer(famp(ind_part(j)))
  end do

  ! Gather 3-force
  ff(1:np,1:ndim)=0.0D0
  if(hydro.and.tracer)then
     do ind=1,twotondim
        do idim=1,ndim
           do j=1,np
              if (classical_tracer(j)) then
                 ff(j,idim)=ff(j,idim) + &
                      uold(indp(j,ind),idim+1)/max(uold(indp(j,ind),1),smallr)*vol(j,ind)
              end if
           end do
        end do
     end do
  endif
  if(poisson)then
     do ind=1,twotondim
        do idim=1,ndim
           do j=1,np
              ff(j,idim)=ff(j,idim)+f(indp(j,ind),idim)*vol(j,ind)
           end do
        end do
#ifdef OUTPUT_PARTICLE_POTENTIAL
        do j=1,np
           ptcl_phi(ind_part(j)) = phi(indp(j,ind))
        end do
#endif
     end do
  endif

  ! Update velocity
  do idim=1,ndim
     if(static)then
        do j=1,np
           new_vp(j,idim)=ff(j,idim)
        end do
     else
        do j=1,np
           if (classical_tracer(j)) then
              new_vp(j,idim)=ff(j,idim)
           else
              new_vp(j,idim)=vp(ind_part(j),idim)+ff(j,idim)*0.5D0*dtnew(ilevel)
           end if
        end do
     endif
  end do

  ! For sink cloud particle only
  if(sink)then
     do j=1,np
        isink=-idp(ind_part(j))
        if(isink>0.and.tp(ind_part(j)).eq.0d0)then
           do idim=1,ndim
              new_vp(j,idim)=vsink(isink,idim)+ff(j,idim)*0.5D0*dtnew(ilevel)
           enddo
        endif
     end do
  end if

  ! Store velocity
  do idim=1,ndim
     do j=1,np
        vp(ind_part(j),idim)=new_vp(j,idim)
     end do
  end do

  ! Update sink particle velocity using closest cloud particle
  if(sink)then
     do j=1,np
        isink=-idp(ind_part(j))
        if(isink>0.and.tp(ind_part(j)).eq.0d0)then
           r2=(xp(ind_part(j),1)-xsink(isink,1))**2
#if NDIM>1
           r2=(xp(ind_part(j),2)-xsink(isink,2))**2+r2
#endif
#if NDIM>2
           r2=(xp(ind_part(j),3)-xsink(isink,3))**2+r2
#endif
           if(r2<(1d-15*dx_min_loc)**2d0)then
              vsink_new(isink,1:ndim)=vp(ind_part(j),1:ndim)
              oksink_new(isink)=1.0
           end if
           sink_stat(isink,ilevel,ndim*2+1)=sink_stat(isink,ilevel,ndim*2+1)+1d0
           do idim=1,ndim
              xx=xp(ind_part(j),idim)+vp(ind_part(j),idim)*dtnew(ilevel)-xsink(isink,idim)
              if(xx>scale*xbound(idim)/2.0)then
                 xx=xx-scale*xbound(idim)
              endif
              if(xx<-scale*xbound(idim)/2.0)then
                 xx=xx+scale*xbound(idim)
              endif
              sink_stat(isink,ilevel,idim     )=sink_stat(isink,ilevel,idim     )+xsink(isink,idim)+xx
              sink_stat(isink,ilevel,idim+ndim)=sink_stat(isink,ilevel,idim+ndim)+vp(ind_part(j),idim)
           enddo
       endif
     end do
  end if

  ! Update position
  do idim=1,ndim
     if(static)then
        do j=1,np
           new_xp(j,idim)=xp(ind_part(j),idim)
        end do
     else
        do j=1,np
           new_xp(j,idim)=xp(ind_part(j),idim)+new_vp(j,idim)*dtnew(ilevel)
        end do
     endif
  end do

  do idim=1,ndim
     do j=1,np
        xp(ind_part(j),idim)=new_xp(j,idim)
     end do
  end do

end subroutine move1
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine move_tracer(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use poisson_commons
  use hydro_commons, only: unew, fluxes
  use hooks, only: safe_move, relative_level, get_cells_on_face
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !------------------------------------------------------------
  ! This routine moves the tracer following the fluxes
  ! This routine is called by move_fine.
  !------------------------------------------------------------
  integer::i,j,idim,nx_loc
  real(dp)::dx,scale
  ! Grid-based arrays
  real(dp),dimension(1:nvector,1:ndim),save::x0
  ! Particle-based arrays
  real(dp),dimension(1:nvector,1:ndim),save::new_xp,old_xp
  real(dp),dimension(1:nvector,1:3),save::x
  real(dp),dimension(1:3)::skip_loc

  ! MC tracer
  real(dp),dimension(1:nvector,1:twondim,1:twotondim),save::flux
  real(dp),dimension(1:nvector,1:twondim),save::flux_part
  real(dp),dimension(1:nvector),save::rand1, rand2, rand3, mass
  real(dp),dimension(1:nvector,1:ndim),save::proba_correction
  real(dp),dimension(1:twotondim/2)::neighborflux
  integer,dimension(1:twotondim/2)::ncell
  real(dp)::proba1, proba2, proba3
  integer::ison,ipart,iskip,dir,ndir,itmp
  integer,dimension(1:nvector, 0:twondim) :: ind_ngrid
  integer,dimension(1:nvector, 1:twotondim) :: ind_cell
  integer,dimension(1:nvector) :: ind_parent_part, ison_part, ind_ggrid_part
  integer,dimension(1:nvector, 1:twotondim, 0:twondim) :: ind_ncell
  integer,dimension(1:nvector, 0:twondim) :: tmp_ncell
  integer, dimension(1:nvector) :: tmp_ind_cell
  integer, dimension(1:twotondim/2) :: tmp_ind_ncell
  integer, dimension(1:nvector, 1:twotondim, 1:twondim) :: rel_lvl
  logical, dimension(1:nvector) :: star_tracer_flag, sink_tracer_flag, gas_tracer_flag
  integer, dimension(1:nvector) :: new_partp
  real(dp) :: rand, rtmp ! temporary real
  real(dp), dimension(1:nvector) :: outflux, factor
  logical, dimension(1:nvector) :: move

  integer :: ix, iy, iz, fam

  ! Mesh spacing in that level
  dx = 0.5D0**ilevel
  nx_loc = (icoarse_max - icoarse_min + 1)
  skip_loc = (/0.0d0, 0.0d0, 0.0d0/)
  if (ndim > 0) skip_loc(1) = dble(icoarse_min)
  if (ndim > 1) skip_loc(2) = dble(jcoarse_min)
  if (ndim > 2) skip_loc(3) = dble(kcoarse_min)
  scale = boxlen / dble(nx_loc)

  !=======================================!
  ! Grid specific code                    !
  !=======================================!
  ! Get neighbor grids
  call getnborgrids(ind_grid, ind_ngrid, ng)

  ! Compute the index of the cells of each grid
  do ison = 1, twotondim
     iskip = ncoarse + (ison - 1)*ngridmax
     do j = 1, ng
        ind_cell(j, ison) = ind_grid(j) + iskip
     end do
  end do

  ! Get each cell's neighbour in all 6 directions
  do ison = 1, twotondim
     do j = 1, ng
        tmp_ind_cell(j) = ind_cell(j, ison)
     end do
     call getnborfather(tmp_ind_cell, tmp_ncell, ng, ilevel)
     do dir = 0, twondim
        do j = 1, ng
           ind_ncell(j, ison, dir) = tmp_ncell(j, dir)
        end do
     end do
  end do

  ! Loop over dimension
  do ison = 1, twotondim
     iskip = ncoarse + (ison-1)*ngridmax

     ! Compute level of cell 'ison' in direction 'dir'
     do dir = 1, twondim
        do j = 1, ng
           call relative_level(ind_cell(j, ison), &
                ind_ngrid(j, 0:twondim), &
                ind_ncell(j, ison, 0:twondim), dir, &
                rel_lvl(j, ison, dir))
        end do
     end do

     ! Get the flux for the cells in the grid
     do dir = 1, twondim
        do j = 1, ng
           flux(j, dir, ison) = fluxes(ind_cell(j, ison), dir)
        end do
     end do
  end do

  !=======================================!
  ! Particle specific code                !
  !=======================================!
  ! Create global index of grid
  do ipart = 1, np
     ind_ggrid_part(ipart) = ind_grid(ind_grid_part(ipart))
  end do

  ! Generate a random number for each particle
  do ipart = 1, np
     call ranf(localseed, rand1(ipart))
     call ranf(localseed, rand2(ipart))
     call ranf(localseed, rand3(ipart))
  end do

  ! Initialize flag star and indicator in grid
  do ipart = 1, np
     ! Get the parent cell
     fam = famp(ind_part(ipart))
     star_tracer_flag(ipart) = is_star_tracer(fam)
     sink_tracer_flag(ipart) = is_sink_tracer(fam)
     gas_tracer_flag(ipart) = is_MC_tracer(fam)
  end do

  ! Tag the particles to be moved if not already moved
  do ipart = 1, np
     move(ipart) = move_flag(ind_part(ipart))
  end do

  !=======================================!
  ! Force particles to be attached        !
  !=======================================!
  do idim = 1, ndim
     do j = 1, ng
        x0(j, idim) = xg(ind_grid(j), idim)
     end do
  end do

  ! Compute the location of the particle relative to its grid
  do idim = 1, ndim
     do ipart = 1, np
        if (gas_tracer_flag(ipart)) then
           ! Get the location in the grid in dx unit from center
           x(ipart, idim) = xp(ind_part(ipart), idim) / scale + skip_loc(idim)
           x(ipart, idim) = x(ipart, idim) - x0(ind_grid_part(ipart), idim)
           x(ipart, idim) = x(ipart, idim) / dx
        end if
     end do
  end do

  ! Reset ison_part to 1
  do ipart = 1, np
     ison_part(ipart) = 1
  end do

  ! Move particles to cell centers
  do idim = 1, ndim
     do ipart = 1, np
        if (gas_tracer_flag(ipart)) then
           ! Particle in the center of the grid
           if (x(ipart, idim) == 0.0D0) then
              call ranf(localseed, rand)

              ! Project the particle either to the right or the left
              if (rand < 0.5) then
                 ! do nothing
                 x(ipart, idim) = -0.5D0
              else
                 ! ison_part += 2^idim / 2
                 x(ipart, idim) = +0.5D0
                 ison_part(ipart) = ison_part(ipart) + 2**(idim-1)
              end if

           else if (x(ipart, idim) < 0.0D0) then
              x(ipart, idim) = -0.5D0
           else if (x(ipart, idim) > 0.0D0) then
              x(ipart, idim) = +0.5D0
              ison_part(ipart) = ison_part(ipart) + 2**(idim-1)
           end if
        end if
     end do
  end do

  ! Recompute the index of the parent cell
  do ipart = 1, np
     if (gas_tracer_flag(ipart)) then
        iskip = ncoarse + (ison_part(ipart) - 1)*ngridmax
        new_partp(ipart) = ind_ggrid_part(ipart) + iskip
        ind_parent_part(ipart) = new_partp(ipart)
     else
        ! Keep the old ones
        new_partp(ipart) = partp(ind_part(ipart))
        ind_parent_part(ipart) = new_partp(ipart)
     end if
  end do

  ! Get the flux for each gas particle
  do ipart = 1, np
     if (gas_tracer_flag(ipart)) then
        do dir = 1, twondim
           flux_part(ipart, dir) = flux(ind_grid_part(ipart), dir, ison_part(ipart))
        end do
     end if
  end do

  do ipart = 1, np
     factor(ipart) = 1.0D0
  end do

  !==========================================!
  ! Move tracers attached to stars and sinks !
  !==========================================!
  do ipart = 1, np
     if (star_tracer_flag(ipart)) then
        new_xp(ipart, 1:ndim) = xp(ind_parent_part(ipart), 1:ndim)
        vp(ipart, 1:ndim) = vp(ind_parent_part(ipart), 1:ndim)
     else if (sink_tracer_flag(ipart)) then
        ! TODO: xsink or xsink_new?
        new_xp(ipart, 1:ndim) = xsink(ind_parent_part(ipart), 1:ndim)
        vp(ipart, 1:ndim) = vsink(ind_parent_part(ipart), 1:ndim)
     end if
  end do

  !=======================================!
  ! Move tracers attached to grid         !
  !=======================================!
  ! Compute the outgoing fluxes for each particle in a cell + mass of cell
  outflux(1:np) = 0
  do dir = 1, twondim
     do ipart = 1, np
        if (gas_tracer_flag(ipart)) then
           rtmp = flux_part(ipart, dir)
           if (rtmp < 0) outflux(ipart) = outflux(ipart) + rtmp
          end if
     end do
  end do

  ! Prefetch the mass and the local velocity
  do ipart = 1, np
     if (gas_tracer_flag(ipart)) then
        mass(ipart) = unew(ind_parent_part(ipart), 1)
     end if
  end do

  ! Compute correction factor for probability of moving
  proba_correction = 1.0d0

  ! for each direction ...
  do dir = 1, twondim
     ! 'Reverse' the direction
     if (mod(dir, 2) == 1) then ! 1<->2, 3<->4, 5<->6
        ndir = dir + 1
     else
        ndir = dir - 1
     end if
     ! Get the index in grid of cells on the common face of neighbor grid
     call get_cells_on_face(ndir, tmp_ind_ncell(1:twotondim/2))

     ! ... for each particle ...
     do ipart = 1, np
        ! ... not attached to a star
        if (gas_tracer_flag(ipart)) then

           proba1 = -outflux(ipart) / mass(ipart)

           ! The particle is going to be moved
           move(ipart) = move(ipart) .and. (rand1(ipart) < proba1)

           if (move(ipart)) then
              proba2 = flux_part(ipart, dir)/outflux(ipart) * proba_correction(ipart, 1+(dir-1)/2)

              ! Compute probability of moving in direction 'dir'
              if (rand2(ipart) < proba2 .and. move(ipart)) then
                 ! Tag the particle as moved
                 move(ipart) = .false.

                 ! Store the relative level of the neighbor cell
                 itmp = rel_lvl(ind_grid_part(ipart), ison_part(ipart), dir)

                 ! === Move to coarser === !
                 if (itmp == -1) then
                    factor(ipart) = 2.0D0
                    new_partp(ipart) = ind_ncell(ind_grid_part(ipart), ison_part(ipart), dir)

                 ! === Move to same level === !
                 else if (itmp == 0) then
                    factor(ipart) = 1.0D0
                    new_partp(ipart) = ind_ncell(ind_grid_part(ipart), ison_part(ipart), dir)


                 ! === Move to finer === !
                 else
                    factor(ipart) = 0.5D0
                    ! Get the fine-to-coarse flux (and invert the sign)
                    do i = 1, twotondim/2
                       ! Compute the neighbor cell index
                       iskip = ncoarse + (tmp_ind_ncell(i)-1)*ngridmax
                       ncell(i) = son(ind_ncell(ind_grid_part(ipart), ison_part(ipart), dir)) &
                            + iskip
                    end do

                    ! Compute the flux from neighbor cell
                    do i = 1, twotondim/2
                       neighborflux(i) = -fluxes(ncell(i), ndir)/twotondim
                    end do

                    ! Recompute the flux in the direction
                    flux_part(ipart, dir) = sum(neighborflux)

                    ! TODO: fix this!
                    if (flux_part(ipart, dir) == 0) then
                       flux_part(ipart, dir) = 1
                       do i = 1, twotondim/2
                          neighborflux(i) = 2d0/twotondim
                       end do
                    end if

                    ! Chose randomly the target cell
                    do i = 1, twotondim/2
                       proba3 = neighborflux(i) / flux_part(ipart, dir)

                       if (rand3(ipart) == 0) cycle
                       if (rand3(ipart) < proba3) then
                          new_partp(ipart) = ncell(i)

                          ! Prevent other directions
                          rand3(ipart) = 0
                       else
                          rand3(ipart) = rand3(ipart) - max(0d0, proba3)
                       end if
                    end do

                 end if
              else
                 rand2(ipart) = rand2(ipart) - max(0d0, proba2)
              end if
           end if
        end if
     end do
  end do

  ! Actually move the particles to their new location
  do ipart = 1, np
     if (gas_tracer_flag(ipart)) then
        ! Compute new location in grid + father grid position
        ison_part(ipart) = (new_partp(ipart)-ncoarse-1)/ngridmax + 1
        ind_ggrid_part(ipart) = new_partp(ipart) - ncoarse - (ison_part(ipart)-1)*ngridmax

        iz = (ison_part(ipart)-1)/4
        iy = (ison_part(ipart)-1-4*iz)/2
        ix = (ison_part(ipart)-1-4*iz-2*iy)

        x(ipart, 1) = (dble(ix)-0.5d0)
        x(ipart, 2) = (dble(iy)-0.5d0)
        x(ipart, 3) = (dble(iz)-0.5d0)

        do idim = 1, ndim
           new_xp(ipart, idim) = (xg(ind_ggrid_part(ipart), idim) - skip_loc(idim) &
                + x(ipart, idim)*dx*factor(ipart)) * scale
        end do
     end if
  end do

  ! Save old positions
  do idim = 1, ndim
     do ipart = 1, np
        old_xp(ipart, idim) = xp(ind_part(ipart), idim)
     end do
  end do

  ! Safely move particles (take care of boundaries)
  do ipart = 1, np
     call safe_move(new_xp(ipart, 1:ndim), old_xp(ipart, 1:ndim), scale)
  end do

  ! Save back the move flag
  do ipart = 1, np
     move_flag(ind_part(ipart)) = move(ipart)
  end do

  ! Update velocity field (mean of previous speed plus current speed)
  do idim = 1, ndim
     do ipart = 1, np
        if (gas_tracer_flag(ipart)) then
           ! Update speed
           vp(ind_part(ipart), idim) = &
                (new_xp(ipart, idim) - old_xp(ipart, idim)) / dtnew(ilevel)
        end if
     end do
  end do

  do idim = 1, ndim
     do ipart = 1, np
        xp(ind_part(ipart), idim) = new_xp(ipart, idim)
     end do
  end do

  ! Store the new parent (here a cell) of the particle
  do ipart = 1, np
     partp(ind_part(ipart)) = new_partp(ipart)
  end do

end subroutine move_tracer

! subroutine move_tracer_exact(flagp, ilevel)
!   ! This routine computes the exact number of particles to be moved in each grid,
!   ! depending on the flux and the number of particles
!   ! and stores it in the array given as argument
!   use amr_commons
!   use pm_commons
!   use hydro_commons, only: fluxes

!   implicit none
!   integer, intent(in) :: ilevel
!   real(dp), intent(out), dimension(1:npartmax) :: flagp

!   integer :: ig, ip, igrid, npart1, ipart, jgrid, ig2, jpart, next_part, npart2
!   integer, dimension(1:nvector) :: ind_grid, parts_in_grid, tracers_in_grid, tracers_direction

!   ! Reset the direction in which to move the particle
!   ! 0 is for: don't move!
!   flagp = 0

!   ! Compute number of particles to move per grid per direction
!   ig = 0
!   igrid = headl(myid, ilevel)
!   do jgrid = 1, numbl(myid, ilevel)
!      npart1 = numbp(igrid)  ! Number of particles in the grid
!      if(npart1 > 0) then
!         npart2 = 0
!         ig = ig + 1
!         ind_grid(ig) = igrid
!         ipart = headp(igrid)
!         ! Loop over particles
!         do jpart = 1, npart1
!            next_part = nextp(ipart)
!            if (MC_tracer .and. famp(ipart) >= FTRACER .and. famp(ipart) /= FTRACER_CLASSIC) then
!               npart2 = npart2 + 1
!            end if
!            ipart = next_part  ! Go to next particle
!         end do
!         ! End loop over particles
!         tracers_in_grid(ig) = npart2
!         parts_in_grid(ig) = npart1

!         if (npart2 == 0) then
!            ig = ig - 1
!         end if

!         if (ig == nvector) then
!            call compute_particles_to_move(ind_grid, parts_in_grid, tracers_in_grid, ig, &
!                 tracers_direction)
!         end if
!      end if
!      igrid = next(igrid)   ! Go to next grid
!   end do

!   if (ig > 0) then
!      call compute_particles_to_move(ind_grid, tracers_in_grid, ig, tracers_direction)
!   end if


! contains
!   subroutine compute_particles_to_move(ind_grid, tracer_in_grid, ig, tracers_direction)
!     ! Computes the number of particles to move given the fluxes
!     ! It stores the number of particles to move

!     integer, intent(in) :: ig ! Number of particles
!     integer, dimension(1:ig), intent(in) :: ind_grid, tracer_in_grid
!     integer, dimension(1:ig), intent(out) :: tracers_direction



!   end subroutine compute_particles_to_move


! end subroutine move_tracer_exact

! subroutine move_tracer2(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
!   use amr_commons
!   use pm_commons
!   use poisson_commons
!   use hydro_commons, only: unew, fluxes
!   use hooks, only: safe_move, relative_level, get_cells_on_face
!   implicit none
!   integer::ng,np,ilevel
!   integer,dimension(1:nvector)::ind_grid
!   integer,dimension(1:nvector)::ind_grid_part,ind_part
!   !------------------------------------------------------------
!   ! This routine moves the tracer following the fluxes
!   ! This routine is called by move_fine.
!   !------------------------------------------------------------
!   integer::i,j,idim,nx_loc
!   real(dp)::dx,scale
!   ! Grid-based arrays
!   real(dp),dimension(1:nvector,1:ndim),save::x0
!   ! Particle-based arrays
!   real(dp),dimension(1:nvector,1:ndim),save::new_xp,old_xp
!   real(dp),dimension(1:nvector,1:3),save::x
!   real(dp),dimension(1:3)::skip_loc

!   ! MC tracer
!   real(dp),dimension(1:nvector,1:twondim,1:twotondim),save::flux
!   real(dp),dimension(1:nvector,1:twondim),save::flux_part
!   real(dp),dimension(1:nvector),save::rand1, rand2, rand3, mass
!   real(dp),dimension(1:nvector,1:ndim),save::gas_v,part_v,delta_v, proba_correction
!   real(dp),dimension(1:twotondim/2)::neighborflux
!   integer,dimension(1:twotondim/2)::ncell
!   real(dp)::proba1, proba2, proba3
!   integer::ison,ipart,iskip,dir,ndir,itmp
!   integer,dimension(1:nvector, 0:twondim) :: ind_ngrid
!   integer,dimension(1:nvector, 1:twotondim) :: ind_cell
!   integer,dimension(1:nvector) :: ind_parent_part, ison_part, ind_ggrid_part
!   integer,dimension(1:nvector, 1:twotondim, 0:twondim) :: ind_ncell
!   integer,dimension(1:nvector, 0:twondim) :: tmp_ncell
!   integer, dimension(1:nvector) :: tmp_ind_cell
!   integer, dimension(1:twotondim/2) :: tmp_ind_ncell
!   integer, dimension(1:nvector, 1:twotondim, 1:twondim) :: rel_lvl
!   logical, dimension(1:nvector) :: is_star
!   integer, dimension(1:nvector) :: new_partp, old_partp
!   real(dp) :: rand, rtmp ! temporary real
!   real(dp), dimension(1:nvector) :: outflux, factor
!   logical, dimension(1:nvector) :: move, ok

!   integer :: ix, iy, iz

!   ! Mesh spacing in that level
!   dx = 0.5D0**ilevel
!   nx_loc = (icoarse_max - icoarse_min + 1)
!   skip_loc = (/0.0d0, 0.0d0, 0.0d0/)
!   if (ndim > 0) skip_loc(1) = dble(icoarse_min)
!   if (ndim > 1) skip_loc(2) = dble(jcoarse_min)
!   if (ndim > 2) skip_loc(3) = dble(kcoarse_min)
!   scale = boxlen / dble(nx_loc)

!   !=======================================!
!   ! Grid specific code                    !
!   !=======================================!
!   ! Get neighbor grids
!   call getnborgrids(ind_grid, ind_ngrid, ng)

!   ! Compute the index of the cells of each grid
!   do ison = 1, twotondim
!      iskip = ncoarse + (ison - 1)*ngridmax
!      do j = 1, ng
!         ind_cell(j, ison) = ind_grid(j) + iskip
!      end do
!   end do

!   ! Get each cell's neighbour in all 6 directions
!   do ison = 1, twotondim
!      do j = 1, ng
!         tmp_ind_cell(j) = ind_cell(j, ison)
!      end do
!      call getnborfather(tmp_ind_cell, tmp_ncell, ng, ilevel)
!      do dir = 0, twondim
!         do j = 1, ng
!            ind_ncell(j, ison, dir) = tmp_ncell(j, dir)
!         end do
!      end do
!   end do

!   ! Loop over dimension
!   do ison = 1, twotondim
!      iskip = ncoarse + (ison-1)*ngridmax

!      ! Compute level of cell 'ison' in direction 'dir'
!      do dir = 1, twondim
!         do j = 1, ng
!            call relative_level(ind_cell(j, ison), &
!                 ind_ngrid(j, 0:twondim), &
!                 ind_ncell(j, ison, 0:twondim), dir, &
!                 rel_lvl(j, ison, dir))
!         end do
!      end do

!      ! Get the flux for the cells in the grid
!      do dir = 1, twondim
!         do j = 1, ng
!            flux(j, dir, ison) = fluxes(ind_cell(j, ison), dir)
!         end do
!      end do
!   end do

!   !=======================================!
!   ! Particle specific code                !
!   !=======================================!
!   ! Create global index of grid
!   do ipart = 1, np
!      ind_ggrid_part(ipart) = ind_grid(ind_grid_part(ipart))
!   end do

!   ! Generate a random number for each particle
!   do ipart = 1, np
!      call ranf(localseed, rand1(ipart))
!      call ranf(localseed, rand2(ipart))
!      call ranf(localseed, rand3(ipart))
!   end do

!   ! Initialize flag star and indicator in grid
!   do ipart = 1, np
!      ! Get the parent cell
!      is_star(ipart) = famp(ind_part(ipart)) == FTRACER_STAR
!   end do

!   ! Tag the particles to be moved if not already moved
!   do ipart = 1, np
!      move(ipart) = move_flag(ind_part(ipart))
!   end do

!   !=======================================!
!   ! Force particles to be attached        !
!   !=======================================!
!   do idim = 1, ndim
!      do j = 1, ng
!         x0(j, idim) = xg(ind_grid(j), idim)
!      end do
!   end do

!   ! Compute the location of the particle relative to its grid
!   do idim = 1, ndim
!      do ipart = 1, np
!         if (.not. is_star(ipart)) then
!            ! Get the location in the grid in dx unit from center
!            x(ipart, idim) = xp(ind_part(ipart), idim) / scale + skip_loc(idim)
!            x(ipart, idim) = x(ipart, idim) - x0(ind_grid_part(ipart), idim)
!            x(ipart, idim) = x(ipart, idim) / dx
!         end if
!      end do
!   end do

!   ! Reset ison_part to 1
!   do ipart = 1, np
!      ison_part(ipart) = 1
!   end do

!   ! Move particles to cell centers
!   do idim = 1, ndim
!      do ipart = 1, np
!         if (.not. is_star(ipart)) then
!            ! Particle in the center of the grid
!            if (x(ipart, idim) == 0.0D0) then
!               call ranf(localseed, rand)

!               ! Project the particle either to the right or the left
!               if (rand < 0.5) then
!                  ! do nothing
!                  x(ipart, idim) = -0.5D0
!               else
!                  ! ison_part += 2^idim / 2
!                  x(ipart, idim) = +0.5D0
!                  ison_part(ipart) = ison_part(ipart) + 2**(idim-1)
!               end if

!            else if (x(ipart, idim) < 0.0D0) then
!               x(ipart, idim) = -0.5D0
!            else if (x(ipart, idim) > 0.0D0) then
!               x(ipart, idim) = +0.5D0
!               ison_part(ipart) = ison_part(ipart) + 2**(idim-1)
!            end if
!         end if
!      end do
!   end do

!   ! Recompute the index of the parent cell
!   do ipart = 1, np
!      if (.not. is_star(ipart)) then
!         iskip = ncoarse + (ison_part(ipart) - 1)*ngridmax
!         new_partp(ipart) = ind_ggrid_part(ipart) + iskip
!         ind_parent_part(ipart) = new_partp(ipart)
!      else
!         new_partp(ipart) = partp(ind_part(ipart))
!         ind_parent_part(ipart) = new_partp(ipart)
!      end if
!   end do

!   ! Get the flux for each gas particle
!   do ipart = 1, np
!      if (.not. is_star(ipart)) then
!         do dir = 1, twondim
!            flux_part(ipart, dir) = flux(ind_grid_part(ipart), dir, ison_part(ipart))
!         end do
!      end if
!   end do

!   do ipart = 1, np
!      factor(ipart) = 1.0D0
!   end do

!   !=======================================!
!   ! Move tracers attached to stars        !
!   !=======================================!
!   do ipart = 1, np
!      if (is_star(ipart)) then
!         new_xp(ipart, 1:ndim) = xp(ind_parent_part(ipart), 1:ndim)
!         vp(ipart, 1:ndim) = vp(ind_parent_part(ipart), 1:ndim)
!      end if
!   end do

!   !=======================================!
!   ! Move tracers attached to grid         !
!   !=======================================!
!   ! Compute the outgoing fluxes for each particle in a cell + mass of cell
!   outflux(1:np) = 0
!   do dir = 1, twondim
!      do ipart = 1, np
!         if (.not. is_star(ipart)) then
!            rtmp = flux_part(ipart, dir)
!            if (rtmp < 0) outflux(ipart) = outflux(ipart) + rtmp
!           end if
!      end do
!   end do

!   ! Prefetch the mass and the local velocity
!   do ipart = 1, np
!      if (.not. is_star(ipart)) then
!         mass(ipart) = unew(ind_parent_part(ipart), 1)
!      end if
!   end do

!   ! Compute correction factor for probability of moving
!   proba_correction = 1.0d0

!   ! for each direction ...
!   do dir = 1, twondim
!      ! 'Reverse' the direction
!      if (mod(dir, 2) == 1) then ! 1<->2, 3<->4, 5<->6
!         ndir = dir + 1
!      else
!         ndir = dir - 1
!      end if
!      ! Get the index in grid of cells on the common face of neighbor grid
!      call get_cells_on_face(ndir, tmp_ind_ncell(1:twotondim/2))

!      ! ... for each particle ...
!      do ipart = 1, np
!         ! ... not attached to a star
!         if (.not. is_star(ipart)) then

!            proba1 = -outflux(ipart) / mass(ipart)

!            ! The particle is going to be moved
!            move(ipart) = move(ipart) .and. (rand1(ipart) < proba1)

!            if (move(ipart)) then
!               proba2 = flux_part(ipart, dir)/outflux(ipart) * proba_correction(ipart, 1+(dir-1)/2)

!               ! Compute probability of moving in direction 'dir'
!               if (rand2(ipart) < proba2 .and. move(ipart)) then
!                  ! Tag the particle as moved
!                  move(ipart) = .false.

!                  ! Store the relative level of the neighbor cell
!                  itmp = rel_lvl(ind_grid_part(ipart), ison_part(ipart), dir)

!                  ! === Move to coarser === !
!                  if (itmp == -1) then
!                     factor(ipart) = 2.0D0
!                     new_partp(ipart) = ind_ncell(ind_grid_part(ipart), ison_part(ipart), dir)

!                     ! if (ind_part(ipart) == 29563) print*, ind_part(ipart), 'to coarser', myid

!                  ! === Move to same level === !
!                  else if (itmp == 0) then
!                     factor(ipart) = 1.0D0
!                     new_partp(ipart) = ind_ncell(ind_grid_part(ipart), ison_part(ipart), dir)

!                     ! if (ind_part(ipart) == 29563) print*, ind_part(ipart), 'to same', myid

!                  ! === Move to finer === !
!                  else
!                     ! if (ind_part(ipart) == 29563) print*, ind_part(ipart), 'to finer', myid
!                     factor(ipart) = 0.5D0
!                     ! Get the fine-to-coarse flux (and invert the sign)
!                     do i = 1, twotondim/2
!                        ! Compute the neighbor cell index
!                        iskip = ncoarse + (tmp_ind_ncell(i)-1)*ngridmax
!                        ncell(i) = son(ind_ncell(ind_grid_part(ipart), ison_part(ipart), dir)) &
!                             + iskip
!                     end do

!                     ! Compute the flux from neighbor cell
!                     do i = 1, twotondim/2
!                        neighborflux(i) = -fluxes(ncell(i), ndir)/twotondim
!                     end do

!                     ! Recompute the flux in the direction
!                     flux_part(ipart, dir) = sum(neighborflux)

!                     ! TODO: fix this!
!                     if (flux_part(ipart, dir) == 0) then
!                        flux_part(ipart, dir) = 1
!                        do i = 1, twotondim/2
!                           neighborflux(i) = 2d0/twotondim
!                        end do
!                     end if

!                     ! Chose randomly the target cell
!                     do i = 1, twotondim/2
!                        proba3 = neighborflux(i) / flux_part(ipart, dir)

!                        if (rand3(ipart) == 0) cycle
!                        if (rand3(ipart) < proba3) then
!                           new_partp(ipart) = ncell(i)

!                           ! Prevent other directions
!                           rand3(ipart) = 0
!                        else
!                           rand3(ipart) = rand3(ipart) - max(0d0, proba3)
!                        end if
!                     end do

!                  end if
!               else
!                  rand2(ipart) = rand2(ipart) - max(0d0, proba2)
!               end if
!            end if
!         end if
!      end do
!   end do

!   ! Actually move the particles to their new location
!   do ipart = 1, np
!      if (.not. is_star(ipart)) then
!         ! Compute new location in grid + father grid position
!         ison_part(ipart) = (new_partp(ipart)-ncoarse-1)/ngridmax + 1
!         ind_ggrid_part(ipart) = new_partp(ipart) - ncoarse - (ison_part(ipart)-1)*ngridmax

!         iz = (ison_part(ipart)-1)/4
!         iy = (ison_part(ipart)-1-4*iz)/2
!         ix = (ison_part(ipart)-1-4*iz-2*iy)

!         x(ipart, 1) = (dble(ix)-0.5d0)
!         x(ipart, 2) = (dble(iy)-0.5d0)
!         x(ipart, 3) = (dble(iz)-0.5d0)

!         do idim = 1, ndim
!            new_xp(ipart, idim) = (xg(ind_ggrid_part(ipart), idim) - skip_loc(idim) &
!                 + x(ipart, idim)*dx*factor(ipart)) * scale
!         end do
!      end if
!   end do

!   ! Save old positions
!   do idim = 1, ndim
!      do ipart = 1, np
!         old_xp(ipart, idim) = xp(ind_part(ipart), idim)
!      end do
!   end do

!   ! Safely move particles (take care of boundaries)
!   do ipart = 1, np
!      call safe_move(new_xp(ipart, 1:ndim), old_xp(ipart, 1:ndim), scale)
!   end do

!   ! Save back the move flag
!   do ipart = 1, np
!      move_flag(ind_part(ipart)) = move(ipart)
!   end do

!   ! Update velocity field (mean of previous speed plus current speed)
!   do idim = 1, ndim
!      do ipart = 1, np
!         ! Update speed
!         vp(ind_part(ipart), idim) = &
!              (new_xp(ipart, idim) - old_xp(ipart, idim)) / dtnew(ilevel)
!      end do
!   end do

!   do idim = 1, ndim
!      do ipart = 1, np
!         xp(ind_part(ipart), idim) = new_xp(ipart, idim)
!      end do
!   end do

!   ! Store the new parent (here a cell) of the particle
!   do ipart = 1, np
!      partp(ind_part(ipart)) = new_partp(ipart)
!   end do

! end subroutine move_tracer2
