!################################################################
!################################################################
!################################################################
!################################################################
subroutine add_list(ind_part,ind_grid,ok,np)
  use amr_commons
  use pm_commons
  implicit none
  integer, intent(in )::np
  integer,dimension(1:nvector), intent(in)::ind_part,ind_grid
  logical,dimension(1:nvector), intent(in)::ok
  !
  ! Add particles to their new linked lists
  !
  integer::j

  do j = 1, np
     if (ok(j)) then
        if (numbp(ind_grid(j)) > 0) then
           ! Add particle at the tail of its linked list
           nextp(tailp(ind_grid(j))) = ind_part(j)
           prevp(ind_part(j)) = tailp(ind_grid(j))
           nextp(ind_part(j)) = 0
           tailp(ind_grid(j)) = ind_part(j)
           numbp(ind_grid(j)) = numbp(ind_grid(j)) + 1
        else
           ! Initialise linked list
           headp(ind_grid(j)) = ind_part(j)
           tailp(ind_grid(j)) = ind_part(j)
           prevp(ind_part(j)) = 0
           nextp(ind_part(j)) = 0
           numbp(ind_grid(j)) = 1
        end if
     end if
  end do

end subroutine add_list

subroutine add_list_auto(ipart, igrid, flush)
  use amr_commons, only : nvector
  implicit none

  ! Add a particle to the list
  integer, intent(in) :: ipart, igrid
  logical, intent(in) :: flush

  integer, dimension(1:nvector), save :: ind_part, ind_grid
  logical, dimension(1:nvector), save :: ok
  integer, save :: ibuf = 0

  if (flush) then
     if (ibuf > 0) then
        call add_list(ind_part, ind_grid, ok, ibuf)
        ibuf = 0
     end if
  else
     ibuf = ibuf + 1
     ind_part(ibuf) = ipart
     ind_grid(ibuf) = igrid
     ok(ibuf) = .true.
     if (ibuf == nvector) then
        call add_list(ind_part, ind_grid, ok, nvector)
        ibuf = 0
     end if
  end if

end subroutine add_list_auto

!################################################################
!################################################################
!################################################################
!################################################################
subroutine add_free(ind_part,np)
  use amr_commons
  use pm_commons
  implicit none
  integer, intent(in)::np
  integer,dimension(1:nvector), intent(in)::ind_part
  !
  ! Add particles to the free memory linked list
  ! and reset all particle variables
  !
  integer::j,idim

  do idim=1,ndim
     do j=1,np
        xp(ind_part(j),idim)=0.0
        vp(ind_part(j),idim)=0.0
     end do
  end do
  do j=1,np
     mp(ind_part(j))=0.0
     if(use_initial_mass)mp0(ind_part(j))=0.0
     idp(ind_part(j))=0
     levelp(ind_part(j))=0
     famp(ind_part(j))=FUNSET
  end do
  if(star.or.sink)then
     do j=1,np
        tp(ind_part(j))=0.0
        if(write_stellar_densities) then
           st_n_tp(ind_part(j))=0.0
           st_n_SN(ind_part(j))=0.0
           st_e_SN(ind_part(j))=0.0
        endif
     end do
     if(metal)then
        do j=1,np
           zp(ind_part(j))=0.0
        end do
     end if
#ifdef NTARCEGROUPS
     do j=1,np
        ptracegroup(ind_part(j))=0.0
     end do
#endif
  end if

  do j=1,np
     if(numbp_free>0)then
        ! Add particle at the tail of its linked list
        nextp(tailp_free)=ind_part(j)
        prevp(ind_part(j))=tailp_free
        nextp(ind_part(j))=0
        tailp_free=ind_part(j)
        numbp_free=numbp_free+1
     else
        ! Initialise linked list
        headp_free=ind_part(j)
        tailp_free=ind_part(j)
        prevp(ind_part(j))=0
        nextp(ind_part(j))=0
        numbp_free=1
     end if
  end do
  npart=npartmax-numbp_free

end subroutine add_free
!################################################################
!################################################################
!################################################################
!################################################################
subroutine add_free_cond(ind_part,ok,np)
  use amr_commons
  use pm_commons
  implicit none
  integer::np
  integer,dimension(1:nvector)::ind_part
  logical,dimension(1:nvector)::ok
  !
  ! Add particles to the free memory linked list
  ! and reset all particle variables
  !
  integer::j,idim

  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           xp(ind_part(j),idim)=0.0
           vp(ind_part(j),idim)=0.0
        endif
     end do
  end do
  do j=1,np
     if(ok(j))then
        mp(ind_part(j))=0.0
        if(use_initial_mass) mp0(ind_part(j))=0.0
        idp(ind_part(j))=0
        levelp(ind_part(j))=0
        famp(ind_part(j))=FUNSET
     endif
  end do
  if(star.or.sink)then
     do j=1,np
        if(ok(j))then
           tp(ind_part(j))=0.0
           if(write_stellar_densities) then
              st_n_tp(ind_part(j))=0.0
              st_n_SN(ind_part(j))=0.0
              st_e_SN(ind_part(j))=0.0
           endif
        endif
     end do
     if(metal)then
        do j=1,np
           if(ok(j))then
              zp(ind_part(j))=0.0
           endif
        end do
     end if
#ifdef NTRACEGROUPS
     do j=1,np
        if(ok(j))then
           ptracegroup(ind_part(j))=0.0
        endif
     end do
#endif
  end if

  do j=1,np
     if(ok(j))then
        if(numbp_free>0)then
           ! Add particle at the tail of its linked list
           nextp(tailp_free)=ind_part(j)
           prevp(ind_part(j))=tailp_free
           nextp(ind_part(j))=0
           tailp_free=ind_part(j)
           numbp_free=numbp_free+1
        else
           ! Initialise linked list
           headp_free=ind_part(j)
           tailp_free=ind_part(j)
           prevp(ind_part(j))=0
           nextp(ind_part(j))=0
           numbp_free=1
        end if
     endif
  end do
  npart=npartmax-numbp_free

end subroutine add_free_cond

subroutine add_free_auto(ipart, flush)
  use amr_commons, only : nvector
  implicit none

  ! Add a particle to the list
  integer, intent(in) :: ipart
  logical, intent(in) :: flush

  integer, dimension(1:nvector), save :: ind_part
  integer, save :: ibuf = 0

  if (flush) then
     if (ibuf > 0) then
        call add_free(ind_part, ibuf)
        ibuf = 0
     end if
  else
     ibuf = ibuf + 1
     ind_part(ibuf) = ipart
     if (ibuf == nvector) then
        call add_free(ind_part, nvector)
        ibuf = 0
     end if
  end if

end subroutine add_free_auto
