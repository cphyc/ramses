#!/usr/bin/env python

import yt
import numpy as np

import argparse

parser = argparse.ArgumentParser(description='Generate initial conditions for tracer particles.')
parser.add_argument('N', type=int, default=int(1e6),
                    help='Number of particles to draw (default: %default)')
parser.add_argument('-i', '--input', type=str,
                    default='output_00001/info_00001.txt',
                    help='Path to the initial condition file (default: %default)')
parser.add_argument('-o', '--output', type=str,
                    default='ic_tracers',
                    help='Name of the output file (ascii, default: %default)')

args = parser.parse_args()

yt.enable_parallelism()

Ntracer = args.N
input_fname = args.input
output_fname = args.output

ds = yt.load(input_fname)
ad = ds.all_data()

# Compute gas mass
gas_mass = ad['cell_mass'].sum()
mtracer = gas_mass / Ntracer

# Get position and mass of the cells
x, y, z = xyz = np.array([ad['index', k] for k in 'xyz'])
mass = ad['cell_mass']


# Compute number of particle in cell
n_in_cell = mass / mtracer


print('adding particles in cells containing more than 1 particles (%s loops)' % np.floor(n_in_cell.max().value).astype(int))
pos = np.zeros((0, 3))
while any(n_in_cell > 1):
    print('\tloop: %s' % np.floor(n_in_cell.max().value).astype(int))
    w = n_in_cell >= 1

    newpos = xyz[:, w]
    n_in_cell[w] = n_in_cell[w] - 1

    pos = np.concatenate((pos, newpos.T))

print('adding particles in cells containg more than 0 particles')
d = np.random.rand(n_in_cell.shape[0])
w = d < n_in_cell

newpos = xyz[:, w]
pos = np.concatenate((pos, newpos.T))

vel = np.zeros_like(pos)
m = np.zeros((pos.shape[0], 1))

data = np.concatenate((pos, vel, m), axis=-1)

print('saving to %s, %s particles' % (output_fname, pos.shape[0]))
np.savetxt(output_fname, data, fmt='%.4e %.4e %.4e %.0f %.0f %.0f %.0f'.split())
