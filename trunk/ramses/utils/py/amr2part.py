# -*- coding: utf-8 -*-
import numpy as np
import argparse
from scipy.io import FortranFile as FF
import itertools
import sys
try:
    from tqdm import tqdm
except ImportError:
    def tqdm(it, *args, **kwargs):
        return it

parser = argparse.ArgumentParser(description='Convert cube data into particle data')
parser.add_argument('-i', '--input', help='Input cube',
                    type=str, required=True)
parser.add_argument('-o', '--output', help='Output file (particle list)',
                    type=str, required=True)
parser.add_argument('-n', '--npc', help='Minimum number per cell (default %(default)s)', type=int,
                    default=1)
parser.add_argument('--max-n', help='Maximum number of particles per cell (overrides -n)', type=int,
                    default=-1)
parser.add_argument('--dry', help='Execute a dry run (do not do anything)', action='store_true',
                    default=False)
args = parser.parse_args()

print('Reading input "%s"' % args.input)
f = FF(args.input)
nx, ny, nz = ndims = f.read_ints()

dens = f.read_reals(dtype=np.float32).reshape(ndims)

print('Generate the particles')
if args.max_n:
    dmax = dens.max()
    npart_arr = dens/dmax*args.max_n
else:
    dmin = dens.min()
    npart_arr = dens/dmin*args.npc

if args.dry:
    print('Will create between %s and %s particles on a %s grid' % (
        np.floor(npart_arr).sum().astype(int),
        np.ceil(npart_arr).sum().astype(int),
        (nx, ny, nz)
    ))
    sys.exit(0)

Ntot = 0
with open(args.output, 'w') as f:
    for i, j, k in tqdm(itertools.product(range(nx), range(ny), range(nz)),
                        total=nx*ny*nz):

        x = (i+0.5)/nx
        y = (j+0.5)/ny
        z = (k+0.5)/nz

        Noix = lambda : (np.random.rand()-0.5)/nx
        Noiy = lambda : (np.random.rand()-0.5)/ny
        Noiz = lambda : (np.random.rand()-0.5)/nz

        npart = npart_arr[i, j, k]

        # randomly pick a particle number
        proba = 1 - (npart - np.floor(npart))
        r = np.random.rand()

        if r < proba:
            N = np.floor(npart).astype(int)
        else:
            N = np.floor(npart).astype(int) + 1

        Ntot += N

        [f.write('%15.14f %15.14f %15.14f %15.14f %15.14f %15.14f %15.14f\n'
                 % (x+Noix(), y+Noiy(), z+Noiz(), 0, 0, 0, 1.0))
         for i in range(N)]
        f.write('')

print('Created %s particles' % Ntot)
